package com.collegelasalle.hui.quizzapp;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class QuizFragment extends Fragment implements View.OnClickListener {

    private NavController navController;
    private FirebaseFirestore firebaseFirestore;
    private String currentUserId;
    private FirebaseAuth firebaseAuth;

    private String quizName;
    private String quizId;

    private static final String TAG = "QUIZ_FRAGMENT_LOG";
    private CountDownTimer countDownTimer;


    /* UI elements */
    private TextView quizTitle;
    private Button optionOneBtn;
    private Button optionTwoBtn;
    private Button optionThreeBtn;
    private Button nextBtn;
    private ImageButton closeBtn;
    private TextView questionFeedback;
    private TextView questionText;
    private TextView questionTime;
    private TextView questionNumber;
    private ProgressBar questionProgress;


    /* firebase data */
    private List<QuestionsModel> allQuestionsList = new ArrayList<>();
    private List<QuestionsModel> questionsToAnswer = new ArrayList<>();
    private long totalQuestionsToAnswer = 0L;

    /* answer question */
    private boolean canAnswer = false;
    private int currentQuestion = 0;
    private int correctAnswers = 0;
    private int wrongAnswers = 0;
    private int notAnswered = 0;


    public QuizFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_quiz, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController = Navigation.findNavController(view);
        firebaseAuth = FirebaseAuth.getInstance();

        /* get user Id */
        if(firebaseAuth.getCurrentUser() != null)
        {
            currentUserId = firebaseAuth.getCurrentUser().getUid();
        }
        else
        {
            /* go back to home page */
        }


        /* initialize */
        firebaseFirestore = FirebaseFirestore.getInstance();

        /* UI initialize */
        quizTitle = view.findViewById(R.id.quiz_title);
        optionOneBtn = view.findViewById(R.id.quiz_option_one);
        optionTwoBtn = view.findViewById(R.id.quiz_option_two);
        optionThreeBtn = view.findViewById(R.id.quiz_option_three);
        nextBtn = view.findViewById(R.id.quiz_next_btn);
        questionFeedback = view.findViewById(R.id.quiz_question_feedback);
        questionText = view.findViewById(R.id.quiz_question);
        questionTime = view.findViewById(R.id.quiz_question_time);
        questionNumber = view.findViewById(R.id.quiz_question_number);
        questionProgress = view.findViewById(R.id.quiz_question_progress);

        /* get quizId */
        quizId = QuizFragmentArgs.fromBundle(getArguments()).getQuizId();
        quizName = QuizFragmentArgs.fromBundle(getArguments()).getQuizName();
        totalQuestionsToAnswer = QuizFragmentArgs.fromBundle(getArguments()).getTotalQuestions();

        /* get all questions from firebase */
        firebaseFirestore.collection("QuizList")
                .document(quizId).collection("Questions")
                .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful())
                {
                    allQuestionsList = task.getResult().toObjects(QuestionsModel.class);
                   // Log.d(TAG,"Questions List:" + allQuestionsList.get(0).getQuestion());
                    /* pick questions */
                    pickQuestions();
                    loadUI();
                }
                else
                {
                    quizTitle.setText("Error Loading Data" + task.getException().getMessage());

                }
            }
        });

        /* set button click */
        optionOneBtn.setOnClickListener(this);
        optionTwoBtn.setOnClickListener(this);
        optionThreeBtn.setOnClickListener(this);
        nextBtn.setOnClickListener(this);


    }

    private void pickQuestions() {
        for(int i=0; i<totalQuestionsToAnswer; i++)
        {
            int randomNumber = getRandomInteger(allQuestionsList.size(),0);
            questionsToAnswer.add(allQuestionsList.get(randomNumber));
            allQuestionsList.remove(randomNumber);
            Log.d(TAG,"Question : " + i + " : " + questionsToAnswer.get(i).getQuestion());
        }
    }

    private void loadUI() {
        /* quiz data loaded, load the UI */
        quizTitle.setText("Loading Question");
        questionText.setText("Load First Question");

        /* Enable option */
        enableOptions();

        /* load first question */
        loadQuestion(1);

    }

    private void loadQuestion(int questNum) {
        /* set question number */
        questionNumber.setText(questNum + "");

        /* load question text */
        questionText.setText(questionsToAnswer.get(questNum-1).getQuestion());

        /* load options */
        optionOneBtn.setText(questionsToAnswer.get(questNum-1).getOption_a());
        optionTwoBtn.setText(questionsToAnswer.get(questNum-1).getOption_b());
        optionThreeBtn.setText(questionsToAnswer.get(questNum-1).getOption_c());

        /* question loaded, set canAnswer */
        canAnswer = true;
        currentQuestion = questNum;


        /* start question timer */
        startTimer(questNum);

    }

    private void startTimer(int questionNumber) {
        /* set timer text */
        final Long timeToAnswer = questionsToAnswer.get(questionNumber-1).getTimer();
        questionTime.setText(timeToAnswer.toString());

        /* show timer progress */
        questionProgress.setVisibility((View.VISIBLE));


        /* start countdown */
        countDownTimer = new CountDownTimer(timeToAnswer*1000, 10)
        {
            @Override
            public void onTick(long millisUntilFinished) {
                /* update time */
                questionTime.setText((millisUntilFinished/1000 + ""));

                /* progress in percent */
                Long percent = millisUntilFinished/(timeToAnswer*10);
                questionProgress.setProgress(percent.intValue());

            }

            @Override
            public void onFinish() {
                /* timer up, can answer questions anymore */
                canAnswer = false;

                questionFeedback.setText("Time Up! No answer was submited.");
                questionFeedback.setTextColor(getResources().getColor(R.color.colorPrimary,null));
                notAnswered++;
                showNextButton();
            }
        };
        countDownTimer.start();
    }


    private void enableOptions()
    {
        /* show all option button */
        optionOneBtn.setVisibility(View.VISIBLE);
        optionTwoBtn.setVisibility(View.VISIBLE);
        optionThreeBtn.setVisibility(View.VISIBLE);
        /* enable all option button */
        optionOneBtn.setEnabled(true);
        optionTwoBtn.setEnabled(true);
        optionThreeBtn.setEnabled(true);
        /* hide feedback and next button */
        questionFeedback.setVisibility(View.INVISIBLE);
        nextBtn.setVisibility(View.INVISIBLE);
        nextBtn.setEnabled(false);
    }



    public static int getRandomInteger(int max, int min)
    {
        return ((int) (Math.random()*(max - min))) + min;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.quiz_option_one:
                verifyAnswer(optionOneBtn);
                break;
            case R.id.quiz_option_two:
                verifyAnswer(optionTwoBtn);
                break;
            case R.id.quiz_option_three:
                verifyAnswer(optionThreeBtn);
                break;
            case R.id.quiz_next_btn:
                if(currentQuestion == totalQuestionsToAnswer)
                {
                    /* load result */
                    submitResults();
                }
                else
                {
                    currentQuestion++;
                    loadQuestion(currentQuestion);
                    resetOptions();
                }
                break;
        }

    }

    private void submitResults() {
        HashMap<String, Object> resultMap = new HashMap<>();
        resultMap.put("Correct", correctAnswers);
        resultMap.put("Wrong", wrongAnswers);
        resultMap.put("Unanswered",notAnswered);

        firebaseFirestore.collection("QuizList")
                .document(quizId).collection("Results")
                .document(currentUserId).set(resultMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful())
                {
                    /* go to results page */
                   QuizFragmentDirections.ActionQuizFragmentToResultFragment action = QuizFragmentDirections.actionQuizFragmentToResultFragment();
                   action.setQuizId((quizId));
                   navController.navigate(action);
                }
                else
                {
                    /* show error */
                    quizTitle.setText(task.getException().getMessage());
                }
            }
        });

    }

    private void resetOptions() {
        optionOneBtn.setBackground(getResources().getDrawable(R.drawable.outline_light_btn_bg,null));
        optionTwoBtn.setBackground(getResources().getDrawable(R.drawable.outline_light_btn_bg,null));
        optionThreeBtn.setBackground(getResources().getDrawable(R.drawable.outline_light_btn_bg,null));

        optionOneBtn.setTextColor(getResources().getColor(R.color.colorLightText,null));
        optionTwoBtn.setTextColor(getResources().getColor(R.color.colorLightText,null));
        optionThreeBtn.setTextColor(getResources().getColor(R.color.colorLightText,null));

        questionFeedback.setVisibility(View.INVISIBLE);
        nextBtn.setVisibility(View.INVISIBLE);
        nextBtn.setEnabled(false);
    }

    private void verifyAnswer(Button selectedAnswerBtn) {

        /* check answer */
        if(canAnswer)
        {
            /* set answer btn text color to black */
            selectedAnswerBtn.setTextColor(getResources().getColor(R.color.colorText,null));

            if(questionsToAnswer.get(currentQuestion-1).getAnswer().equals(selectedAnswerBtn.getText()))
            {
                /* correct answer */
               // Log.d(TAG,"Correct Answer");
                correctAnswers++;
                selectedAnswerBtn.setBackground(getResources().getDrawable(R.drawable.correct_answer_btn_bg, null));

                /* set feedback text */
                questionFeedback.setText("Correct Answer");
                questionFeedback.setTextColor(getResources().getColor(R.color.colorPrimary,null));
            }
            else
            {
                /* wrong answer */
                //Log.d(TAG,"Wrong Answer");
                wrongAnswers++;
                selectedAnswerBtn.setBackground(getResources().getDrawable(R.drawable.wrong_answer_btn_bg, null));

                /* set feedback text */
                questionFeedback.setText("Wrong Answer \n Correct Answer : " + questionsToAnswer.get(currentQuestion-1).getAnswer());
                questionFeedback.setTextColor(getResources().getColor(R.color.colorAccent,null));

            }
            /* set can answer to false */
            canAnswer = false;

            /* stop the timer */
            countDownTimer.cancel();

            /* show next button */
            showNextButton();

        }
    }

    private void showNextButton() {
        if(currentQuestion == totalQuestionsToAnswer)
        {
            nextBtn.setText("Submit Result");
        }
        questionFeedback.setVisibility(View.VISIBLE);
        nextBtn.setVisibility(View.VISIBLE);
        nextBtn.setEnabled(true);
    }


}
