package com.collegelasalle.hui.quizzapp;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

/* observe the live data and update and list the detail fragment */
public class QuizListViewModel extends ViewModel implements FirebaseRepository.OnFirestoreTaskComplete {

    /* keep the mutable data real-time */
    private MutableLiveData<List<QuizListModel>> quizListModelData = new MutableLiveData<>();

    private FirebaseRepository firebaseRepository = new FirebaseRepository(this);

    public LiveData<List<QuizListModel>> getQuizListModelData() {
        return quizListModelData;
    }

    public QuizListViewModel()
    {
        firebaseRepository.getQuizData();
    }

    @Override
    public void quizListDataAdded(List<QuizListModel> quizListModelList) {
        quizListModelData.setValue(quizListModelList);

    }

    @Override
    public void onError(Exception e) {

    }
}


